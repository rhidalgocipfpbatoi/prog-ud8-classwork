/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud8.classwork.activitat2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat2 {
    
    public static void main(String[] args) {
        ArrayList<String> companyeros = new ArrayList<>();
        companyeros.add("Pedro");
        companyeros.add("Nadia");
        companyeros.add("Camila");
        companyeros.add("Adam");
        companyeros.add("Dani");
        companyeros.add("Kevin");
        
        Iterator iterator = companyeros.iterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        
        Scanner scanner = new Scanner(System.in);
        System.out.print("Dime un nombre: ");
        String nombre = scanner.next();
        
        if (companyeros.contains(nombre)) {
            System.out.printf("El compañero %s sí que está\n", nombre);
        } else {
            System.out.println("No es compañero");
        }
    }
    
}
