/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud8.classwork.activitat1;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat1 {
    
    public static void main(String[] args) {
        ArrayList<String> arcoiris = new ArrayList<>();
        arcoiris.add("Rojo");
        arcoiris.add("Amarillo");
        arcoiris.add("Naranja");
        arcoiris.add("Verde");
        arcoiris.add("Azul");
        arcoiris.add("Añil");
        arcoiris.add("Violeta");
        
        // Punto 1
        System.out.printf("El arcoiris tiene %d colores\n", arcoiris.size());
        
        // Punto 2
        if (arcoiris.indexOf("Rojo") != -1) {
            System.out.printf("El color Rojo ocup la posición %d\n", arcoiris.indexOf("Rojo")+1);
        } else {
            System.out.println("El color Rojo no está");
        }
        
        // Punto 3 (dos opciones)
        for (String color : arcoiris) {
            System.out.println(color);
        }
        
        /*for (int i = 0; i < arcoiris.size(); i++) {
            System.out.println(arcoiris.get(i));
        }*/
        
        // Punto 4
        gestionarColor(arcoiris);
            
        for (String color : arcoiris) {
            System.out.println(color);
        }
    }
    
    private static void gestionarColor(ArrayList<String> arcoiris) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Dime el color: ");
        String color = scanner.next();
        
        if (arcoiris.contains(color)) {
            System.out.printf("El color %s sí que está\n", color);
        } else {
            arcoiris.add(color);
            System.out.printf("El color %s no estaba y se añadió\n", color);
        }
    }
    
}
