/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud8.classwork.activitat6;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat6 {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        HashSet<String> nombres = new HashSet<>();
        
        boolean seAcabaBucleExterno = false;
        do {
            System.out.println("Introdueix noms de persones separats per espais");
            do {
                if (scanner.hasNext()) {
                    String nombre = scanner.next();

                    if (nombre.equalsIgnoreCase("FIN")) {
                        seAcabaBucleExterno = true;
                        break;
                    } 

                    nombres.add(nombre);
                } else {
                    break;
                }
            }while(true);
            
        } while(!seAcabaBucleExterno);
        
        for (String nombre : nombres) {
            System.out.println(nombre);
        }
        
        // Opción iterator está en las diapositivas
    }
}
