package prog.ud8.classwork.activitat8;


import java.util.LinkedList;

public class TestForo {

    public static void main(String[] args) {

        LinkedList<Foro> foros = new LinkedList<>();

        Foro basica1 = new Foro("básica 1");
        foros.add(basica1);

        ForoInteligente inteligente1 = new ForoInteligente("inteligente 1", "facebook", "thepiratebay");
        foros.add(inteligente1);

        ForoInteligente inteligente2 = new ForoInteligente("inteligente 2", "foto", "descargar");
        foros.add(inteligente2);

        for (Foro foro: foros) {
            foro.registrarEntrada("¿Habéis utilizado el servicio de Instagram, ayer me saltó un error en el servicio?");
            foro.registrarEntrada("Los de thepiratebay informaron que podría haber sido un ataque");
            foro.registrarEntrada("Algunos usuarios compartieron la publicación en facebook junto con una foto en la que aparecían los causantes del problema. ¡Era un error de programación!");
        }

        for (Foro foro: foros) {
            System.out.println(foro);
        }
    }
}
