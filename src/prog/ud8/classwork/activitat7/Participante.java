/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud8.classwork.activitat7;

import java.util.Objects;

/**
 *
 * @author batoi
 */
public class Participante {
    
    private String nombre;
    private String dni;
    private float tiempo;

    public Participante(String nombre, String dni, float tiempo) {
        this.nombre = nombre;
        this.dni = dni;
        this.tiempo = tiempo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.nombre);
        hash = 71 * hash + Objects.hashCode(this.dni);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Participante other = (Participante) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return Objects.equals(this.dni, other.dni);
    }

    

    @Override
    public String toString() {
        return "Participante{" + "nombre=" + nombre + ", dni=" + dni + ", tiempo=" + tiempo + '}';
    }
    
    
    
}
