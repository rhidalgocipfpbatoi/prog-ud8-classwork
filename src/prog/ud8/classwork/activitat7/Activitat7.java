/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud8.classwork.activitat7;

import java.util.HashSet;

/**
 *
 * @author batoi
 */
public class Activitat7 {
    
    public static void main(String[] args) {
        HashSet<Participante> prueba1 = new HashSet<>();
        prueba1.add(new Participante("Toni García", "12345678A", 12.5f));
        prueba1.add(new Participante("Pepito Palotes", "53212312E", 34.5f));
        System.out.println("Total participantes:" + prueba1.size());
        
        HashSet<Participante> prueba2 = new HashSet<>();
        prueba2.add(new Participante("Toni García", "12345678A", 19f));
        prueba2.add(new Participante("Maruchi Pérez", "1234312E", 1.5f));
        
        HashSet<Participante> unionPruebas = new HashSet<>();
        unionPruebas.addAll(prueba1);
        unionPruebas.addAll(prueba2);
        
        for (Participante unionPrueba : unionPruebas) {
            System.out.println(unionPrueba);
        }
        
        
    }
    
}
