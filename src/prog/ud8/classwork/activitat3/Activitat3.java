/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud8.classwork.activitat3;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author batoi
 */
public class Activitat3 {
    public static void main(String[] args) {
        Random random = new Random();
        ArrayList<Integer> numeros = new ArrayList<>();
        
        for (int i = 0; i < 50; i++) {
            numeros.add(random.nextInt(101));
        }
        
        System.out.printf("La suma es %d\n", calcularSuma(numeros));
        calcularMedia(numeros);
        calcularMaximo(numeros);
        calcularMinimo(numeros);
    }
    
    private static int calcularSuma(ArrayList<Integer> numeros) {
        int suma = 0;
        
        for (Integer numero : numeros) {
            suma += numero;
        }
        
        return suma;
    }
    
    private static void calcularMedia(ArrayList<Integer> numeros) {
        int suma = calcularSuma(numeros);
        System.out.printf("La media es %.1f\n", (double)suma/numeros.size());
    }
    
    private static void calcularMaximo(ArrayList<Integer> numeros) {
        int maximo = Integer.MIN_VALUE;
        for (Integer numero : numeros) {
            if (numero > maximo) {
                maximo = numero;
            }
        }
        
        System.out.printf("El máximo es %d\n", maximo);
    }
    
    private static void calcularMinimo(ArrayList<Integer> numeros) {
        int minimo = Integer.MAX_VALUE;
        for (Integer numero : numeros) {
            if (numero < minimo) {
                minimo = numero;
            }
        }
        
        System.out.printf("El mínimo es %d\n", minimo);
    }
}
