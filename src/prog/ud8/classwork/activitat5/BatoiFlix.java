package prog.ud8.classwork.activitat5;

import prog.ud8.classwork.activitat5.tiposproducciones.Documental;
import prog.ud8.classwork.activitat5.tiposproducciones.Pelicula;
import prog.ud8.classwork.activitat5.tiposproducciones.Serie;



public class BatoiFlix {

    public static void main(String[] args) {

        Date releaseDate = new Date("19/02/2021");
        Documental dreamSongs = new Documental("Dream Songs", 2400l, Format.MPG, releaseDate, "Enrique Juncosa",
                "Movimiento Hippie");
        dreamSongs.setSummary("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        releaseDate = new Date("20/06/2020");
        Pelicula newsOfTheWorld = new Pelicula("News of the World", 7200l, Format.AVI, releaseDate, "Tom Hanks",
                "Carolina Betller");
        newsOfTheWorld.setSummary("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        Date date = new Date("04/06/2001");
        Serie elHacker = new Serie("El hacker", 3600l, Format.FLV, date,
                1, 20);
        elHacker.setSummary("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        System.out.println("\nListado Producciones (Uso toString()) \n");
        System.out.println(newsOfTheWorld);
        System.out.println(dreamSongs);
        System.out.println(elHacker);

        System.out.println("\nDetalle Producciones (uso mostrarDetalle()\n");
        newsOfTheWorld.showDetails();
        dreamSongs.showDetails();
        elHacker.showDetails();

        Cliente cliente1 = new Cliente(new Date("01/05/2001"), "cliente1@a.com", "Pepe", "Pérez", "1111111A");
        Cliente cliente2 = new Cliente(new Date("02/01/1994"), "cliente2@a.com", "Pepa", "Pig", "2222222A");
        Cliente cliente3 = new Cliente(new Date("10/01/1956"), "cliente3@a.com", "Pepo", "Ron", "3333333A");

        Compra compra1 = new Compra(dreamSongs, new Date("12/12/2022"), false, "Genial");
        Compra compra2 = new Compra(elHacker, new Date("03/10/2020"), false, "Estupendo");
        Compra compra3 = new Compra(newsOfTheWorld, new Date("09/04/2020"), false, "Normalita");
        Compra compra4 = new Compra(dreamSongs, new Date("12/01/2022"), false, "Fatal");
        cliente1.anyadirCompra(compra1);
        cliente2.anyadirCompra(compra3);
        cliente3.anyadirCompra(compra2);
        cliente2.anyadirCompra(compra4);

        System.out.println(cliente1);
        System.out.println(cliente2);
        System.out.println(cliente3);

    }

}