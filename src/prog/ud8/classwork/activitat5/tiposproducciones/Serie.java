package prog.ud8.classwork.activitat5.tiposproducciones;

;

import prog.ud8.classwork.activitat5.Date;
import prog.ud8.classwork.activitat5.Format;
import prog.ud8.classwork.activitat5.Format;

public class Serie extends Produccion {

    private int season;

    private int chapter;

    private static final float PRECIO = 24;
    public Serie(String title, long duration, Format format, Date releaseDate, int season, int chapter) {
        super(title, duration, format, releaseDate, PRECIO);
        this.season = season;
        this.chapter = chapter;
    }

    public Serie(String title, Format format, Date releaseDate, String director, int season, int chapter) {
        this(title, 2400l, format, releaseDate, season, chapter);
    }

    public void showDetails() {
        System.out.println("-------------------Serie--------------------");
        super.showDetails();
        System.out.printf("Capitulo: %d - Temporada %d %n", chapter, season);
        System.out.println("Duracion: " + getDurationTime());
        System.out.println("--------------------------------------------");
    }

    @Override
    public String toString() {
        return super.toString() + " (Serie) temporada = " + season + ", capítulo=" + chapter;
    }

}
