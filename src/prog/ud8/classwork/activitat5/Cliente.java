package prog.ud8.classwork.activitat5;

import java.util.ArrayList;

public class Cliente {

    private Date fechaNacimiento;
    private String email;
    private String nombre;
    private String apellidos;
    private String dni;
    private ArrayList<Compra> compras;

    public Cliente(Date fechaNacimiento, String email, String nombre, String apellidos, String dni) {
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.compras = new ArrayList<>();
    }

    public void anyadirCompra(Compra compra) {
        if (!compras.contains(compra)) {
            compras.add(compra);
        }
    }

    public float calcularTotalGastado() {
        float total = 0f;
        for (Compra compra: compras) {
            total += compra.getPrecio();
        }
        return total;
    }

    @Override
    public String toString() {
        return String.format("Cliente: %s %s | Compras(%d): %s - Total Gastado: %.0f€",
                nombre, apellidos, compras.size(), compras, calcularTotalGastado());
    }
}
