package prog.ud8.classwork.activitat5;

import prog.ud8.classwork.activitat5.tiposproducciones.Produccion;


public class Compra {

    private Produccion produccion;
    private Date fechaRealizado;
    private boolean esFinalizado;
    private String comentario;

    public Compra(Produccion produccion, Date fechaRealizado, boolean esFinalizado, String comentario) {
        this.produccion = produccion;
        this.fechaRealizado = fechaRealizado;
        this.esFinalizado = esFinalizado;
        this.comentario = comentario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Compra that)) return false;
        return this.produccion.equals(that.produccion);
    }

    public float getPrecio() {
        return produccion.getPrecio();
    }

    @Override
    public String toString() {
        return String.format("%s - fecha: %s - %s", produccion.getTitulo(), fechaRealizado, comentario);
    }
}
