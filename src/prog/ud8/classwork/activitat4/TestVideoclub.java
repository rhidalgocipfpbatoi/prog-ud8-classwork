/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud8.classwork.activitat4;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class TestVideoclub {
    
    public static void main(String[] args) {
        
        ArrayList<Videojuego> videoclub = new ArrayList<>();
        videoclub.add(new Videojuego("Fornite", "Acción", 40, true));
        videoclub.add(new Videojuego("Fifa", "Deportes", 50, true));
        videoclub.add(new Videojuego("Grand Theft Auto", "Acción", 80, true));
        videoclub.add(new Videojuego("Minecraft", "Simulación", 60, true));
        videoclub.add(new Videojuego("Animalcrossing", "Simulación", 30, true));
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Dime el título:");
        String titulo = scanner.next();
        
        if (videoclub.contains(new Videojuego(titulo))) {
            System.out.println("Sí está");
        } else {
            System.out.println("No está");
        }
    }
}
