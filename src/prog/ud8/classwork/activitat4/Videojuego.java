/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud8.classwork.activitat4;

import java.util.Objects;

/**
 *
 * @author batoi
 */
public class Videojuego {
    
    private String titulo;
    private String genero;
    private int precio;
    private boolean multijugador;

    public Videojuego(String titulo, String genero, int precio, boolean multijugador) {
        this.titulo = titulo;
        this.genero = genero;
        this.precio = precio;
        this.multijugador = multijugador;
    }

    public Videojuego(String titulo) {
        this.titulo = titulo;
    }
    
    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Videojuego other = (Videojuego) obj;
        return Objects.equals(this.titulo, other.titulo);
    }
    
    
}
